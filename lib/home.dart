import 'package:flutter/material.dart';
import 'package:oxygen_measure/menu.dart';
import 'package:oxygen_measure/widgets.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);
  @override
  State<Home> createState() => _HomeState();
}
class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    double w = MediaQuery.of(context).size.width;
    double h = MediaQuery.of(context).size.height;

    return Scaffold(
        body: Stack(
      children: [
        SizedBox(
          height: h,
          width: w,
          child:
              Image.asset("assets/images/liquenportada.jpg", fit: BoxFit.cover),
        ),
        SafeArea(
            child: Container(
             
                padding: const EdgeInsets.only(top: 20),
                color: Colors.amber.withOpacity(0.7),
                height: 100,
                child: Align(
                        child:  Column(
                    children: [
                      text("BIENVENIDOS", 18.0, FontWeight.bold, Colors.black,
                          FontStyle.normal),
                      const SizedBox(
                        height: 10,
                      ),
                      text(
                          "MONITOR DE POLUCIÓN",
                          15.0,
                          FontWeight.bold,
                          Colors.black,
                          FontStyle.normal),
                        
                     
                    ],
                  ),
                  alignment: Alignment.topCenter,
                ))),
        Container(
          width: w,
          margin: EdgeInsets.only(top: h / 1.5),
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(Radius.circular(20)),
              color: Colors.grey.withOpacity(0.8)),
          child: textParagraph(
              "La aplicación mide el nivel de polución en base a imagenes del liquen denominado PARMELIA, encontrado en muchos lugares de Santa Cruz, Bolivia. Por ende, está direccionada para ser utilizada como medidor de nivel de comtaminación en tiempo real.",
              16.0,
              FontWeight.w500,
              Colors.white,
              FontStyle.normal),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: 
          button(
              text("Medir", 15.0, FontWeight.bold, Colors.black,
                  FontStyle.normal),
              context,
              const Menu()),
        )
      ],
    ));
  }
}
