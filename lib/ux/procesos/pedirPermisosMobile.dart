import 'package:permission_handler/permission_handler.dart';

class PedirPermisos{
  void permisos() async {
    Map<Permission, PermissionStatus> statuses = await [
      Permission.storage,
      Permission.camera,
      Permission.photos,
    ].request();
  }
}