import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:oxygen_measure/ux/procesos/pedirPermisosMobile.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:tflite/tflite.dart';

class Menu extends StatefulWidget {
  const Menu({Key? key}) : super(key: key);

  @override
  State<Menu> createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  bool isworking = false;
  String result = '';
  late XFile? fileImgPiker = null;
  final ImagePicker _picker = ImagePicker();
  String path = '';
  TextEditingController text_publicar = TextEditingController();
  String res = '';
  bool swRecargar = false;
runModelFrames() async {
    if (path!= null) {
      var recongnotiions = await Tflite.runModelOnImage(
        path: path,
          imageMean: 127.5,
          imageStd: 127.5,
          numResults: 1,
          threshold: 0.1,
          asynch: true);
      result = "";
      recongnotiions?.forEach((response) {
        result += response['label'].toString();
      });
      setState(() {
        swRecargar=false;
        result;
      });
      isworking = false;
    }
  }
   loadModel() async {
    await Tflite.loadModel(
        model: "assets/model_91.tflite", labels: "assets/labels.txt");
  }

  @override
  void initState() {
    super.initState();
    PedirPermisos().permisos();
    loadModel();
    
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [Color.fromARGB(195, 9, 158, 101), Color.fromARGB(255, 255, 255, 255), Color.fromARGB(255, 5, 78, 138)])),
      child: Scaffold(
        backgroundColor: Colors.transparent,
              appBar: AppBar(
                backgroundColor: Colors.transparent,
                centerTitle: true,
        title: Column(
          children: const [
            Text("MONITOR DE POLUCIÓN", style: TextStyle(fontSize: 15.0,fontFamily: 'Mali', fontWeight: FontWeight.bold),),
            Text("EN BASE A LÍQUEN PARMELIA", style: TextStyle(fontSize: 15.0,fontFamily: 'Mali',),)
          ],
        )
    
      ),
      body: body(),
    ));
  }

  Widget body() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ListView(
        children: [
          Row(),
          if (path.isNotEmpty)
            SizedBox(
              width: 300,
              height: 300,
              child: Image(
                image: FileImage(File(fileImgPiker?.path ?? '')),
                fit: BoxFit.contain,
              ),
            ),
           
          ElevatedButton(
            style: styleButton(Colors.black),
              onPressed: () {
                getImagen(true);
              },
              child: const Text('Galeria', style: TextStyle(fontWeight: FontWeight.w500, fontFamily: 'Mali', ),)),
          ElevatedButton(
            style: styleButton(Colors.black45),
              onPressed: () {
                getImagen(false);
              },
              child: const Text('Camara', style: TextStyle(fontWeight: FontWeight.w500, fontFamily: 'Mali',),)),

          ElevatedButton(
            style: styleButton(Colors.blue.shade800),
            onPressed: (){
              setState(() {
                swRecargar=true;
              });
              runModelFrames();
            },
          child: swRecargar ? const CircularProgressIndicator() : const Text('Enviar', style: TextStyle(fontWeight: FontWeight.w400, fontFamily: 'Mali',),)),
          Container(
            
            margin: const EdgeInsets.only(bottom: 5),
            padding: const EdgeInsets.only(top:10, bottom: 5),
            width: 500,
            height: 40,
            child: Text("Resultado obtenido: "+result, textAlign: TextAlign.center,style: const TextStyle(fontWeight: FontWeight.w600, fontFamily: 'Mali',),),
          ),
          Icon(Icons.check_box, color: (result=="")? Colors.white.withOpacity(0.5):(result=="Amarillo")? Colors.yellow.shade700:(result=="Naranja")? Colors.orange.shade700:(result=="Rojo")? Colors.red.shade800: Colors.green.shade900),
          Container(
            margin: const EdgeInsets.only(top: 10),
            height: 80,
            padding: const EdgeInsets.all(10),
            
            child:Text("${(result=="")?"":'''El resultado obtenido, nos indica que existe'''} ${(result=="")?"":(result=='Amarillo')?"BAJO NIVEL DE POLUCIÓN  \n  Indice de calidad: 26-50":(result=='Naranja')?"NIVEL DE POLUCIÓN MODERADO \n  Indice de calidad: 51-100":(result=='Rojo')?"NIVEL ALTO DE POLUCIÓN. Dañino para grupos sensibles. \n  Indice de calidad: 101 <":"EXISTE UN NIVEL CASI NULO DE POLUCIÓN \n  Indice de calidad: 0-25"}", textAlign: TextAlign.center,style: const TextStyle(fontWeight: FontWeight.w400, fontFamily: 'Mali',),),
   decoration: BoxDecoration(borderRadius: BorderRadius.circular(20.0),color: Colors.white.withOpacity(0.5),
   ),
   
   
          )
          
          
        ],
      ),
    );
  }

  Future<void> getImagen(bool sw) async {
    swRecargar = false;
    fileImgPiker = null;
    path = '';
    fileImgPiker = await _picker.pickImage(
        imageQuality: 10,
        source: sw ? ImageSource.gallery : ImageSource.camera);
    if (fileImgPiker != null) path = fileImgPiker!.path;
    setState(() {});
  }

  Future<bool> pedirPermiso() async {
    var status = await Permission.camera.status;
    if (status.isGranted) {
      return true;
    } else if (status.isDenied) {
      if (await Permission.camera.request().isGranted) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
  ButtonStyle styleButton(color){
    return TextButton.styleFrom(
              backgroundColor: color
            );
  }
}