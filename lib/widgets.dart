import 'package:flutter/material.dart';

text(text, fontSize, fontWeight, color, fontStyle) {
  return Text(text,
      //textAlign: textAlign,
      style: TextStyle(
          color: color,
          fontSize: fontSize,
          fontWeight: fontWeight,
          fontFamily: 'Mali',
          fontStyle: fontStyle));
}

textParagraph(text, fontSize, fontWeight, color, fontStyle) {
  return Text(text,
      textAlign: TextAlign.justify,
      style: TextStyle(
          color: color,
          fontSize: fontSize,
          fontWeight: fontWeight,
          fontFamily: 'Mali',
          fontStyle: fontStyle));
}

button(t, context, route) {
  return ElevatedButton(
        style: TextButton.styleFrom(
            backgroundColor: Colors.transparent,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20))),
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => route));
        },
        child: Container(
          margin: const EdgeInsets.only(bottom: 50),
          padding: const EdgeInsets.only(top: 10,bottom: 10,left: 20,right: 20),
decoration: const BoxDecoration(
  borderRadius: BorderRadius.all(Radius.circular(20)),
    gradient: LinearGradient(
      begin: Alignment.topLeft,
      end: Alignment.bottomRight,
      colors: <Color>[
        Color(0xffee0000),
        Color(0xffeeee00),
      ], 
      tileMode: TileMode.repeated, 
    ),
  ),
          child: t,
        )
        ,
      );
}
